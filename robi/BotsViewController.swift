//
//  BotsViewController.swift
//  robi
//
//  Created by a.skogorev on 06/06/16.
//  Copyright © 2016 givemeapp.co. All rights reserved.
//

import UIKit

// --------------------------------------------------------

class BotsTableViewCell: UITableViewCell {
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var labelTitle: UILabel!
  @IBOutlet weak var labelDescription: UILabel!
}

// --------------------------------------------------------

class BotsViewController: UITableViewController {

  @IBOutlet weak var buttonEdit: UIBarButtonItem!
  @IBOutlet weak var buttonAdd: UIBarButtonItem!
  
  var lastBotAvatar : UIImage? = nil
  var lastBotName : String? = nil
  
  let CELL_REUSE_ID = "BotsTableViewCellId"
  let TO_LOGIN_SEGUE = "SegueLogin"
  let TO_CHAT_SEGUE = "SegueChat"
  
  struct Contact {
    var name : String?
    var desc : String?
    var avatarUrl: String?
  }
  
  var contacts : [Contact] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    buttonAdd.action = #selector(onButtonAddAction)
    buttonEdit.action = #selector(onButtonEditAction)
    
    // Api initialization
    Api.globalInit()
    Api.isAccessAvailable(
      { () -> Void in
        print("Access is available")
        self.refreshContacts()
      },
      
      onForbidden: { () -> Void in
        dispatch_async(dispatch_get_main_queue()) {
          self.performSegueWithIdentifier(self.TO_LOGIN_SEGUE, sender: self)
        }
      },
      
      onError: { () -> Void in
        print("Failed checking access")
      }
    )
  }
  
  func refreshContacts() {
    contacts.removeAll()
    
    Api.getContacts(
      { (contacts : [String: [String: String]]) -> Void in
        self.tableView.beginUpdates()
        
        for (name, info) in contacts {
          self.contacts.append(Contact(
            name: name,
            desc: info["desc"],
            avatarUrl: info["avatar"]
            ))
          
          self.tableView.insertRowsAtIndexPaths([
            NSIndexPath(forRow: self.contacts.count - 1, inSection: 0)
            ], withRowAnimation: .Automatic)
        }
        
        self.tableView.insertRowsAtIndexPaths([
          NSIndexPath(forRow: self.contacts.count - 1, inSection: 0)
          ], withRowAnimation: .Automatic)
        
        self.tableView.endUpdates()
      },
      
      onForbidden: { () -> Void in
        print("Forbidden getting contact list")
      },
      
      onError: { () -> Void in
        print("Failed getting contact list")
    })
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.contacts.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell: BotsTableViewCell
      = self.tableView.dequeueReusableCellWithIdentifier(CELL_REUSE_ID) as! BotsTableViewCell
        
    cell.imgView.layer.cornerRadius = cell.imgView.frame.height / 2
    cell.imgView.layer.masksToBounds = true
    
    Api.getImage(self.contacts[indexPath.row].avatarUrl!,
                 onSuccess: { (image) in
                   cell.imgView.image = image
                 },
                 onError: { () -> Void in
                   print("Error")
                 }
    )
    
    cell.labelTitle.text = self.contacts[indexPath.row].name
    cell.labelDescription.text = self.contacts[indexPath.row].desc
    
    return cell
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    
    let cell = tableView.cellForRowAtIndexPath(indexPath) as! BotsTableViewCell
    lastBotAvatar = cell.imgView.image
    lastBotName = contacts[indexPath.row].name
    
    print("You selected cell number \(indexPath.row).")
    self.performSegueWithIdentifier(self.TO_CHAT_SEGUE, sender: self)
  }
  
  func onButtonAddAction(sender: UIButton!) {
    print("Button tapped")
  }
  
  func onButtonEditAction(sender: UIButton!) {
    print("Button tapped")
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    print("Segue:", segue.identifier)
    switch(segue.identifier) {
      
    case TO_LOGIN_SEGUE?:
      break
      
    case TO_CHAT_SEGUE?:
      let next = (segue.destinationViewController as! ChatViewController)
      next.botAvatar = lastBotAvatar
      next.botName = lastBotName
      
      //next.senderId = "John"
      //next.senderDisplayName = "John"
      break
      
    default:
      // do nothing
      break
    }
  }
}
