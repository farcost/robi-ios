//
//  ChatViewController.swift
//  robi
//
//  Created by a.skogorev on 15/06/16.
//  Copyright © 2016 givemeapp.co. All rights reserved.
//

import Foundation
import JSQMessagesViewController

// Auxil
func dateFor(timeStamp: String) -> NSDate {
  let formater = NSDateFormatter()
  formater.dateFormat = "dd.MM.yyyy HH:mm:ss"
  return formater.dateFromString(timeStamp)!
}

class ChatViewController: JSQMessagesViewController {
  
  let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(
    UIColor(
      red: 10/255,
      green: 180/255,
      blue: 230/255,
      alpha: 1.0)
  )
  
  let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(
    UIColor.lightGrayColor()
  )
  
  var botAvatar : UIImage? = nil
  var botName : String? = nil
  var messages = [JSQMessage]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = botName
    
    self.setup()
    self.getMessages()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func reloadMessagesView() {
    self.collectionView?.reloadData()
  }
}

//MARK - Setup
extension ChatViewController {
  func getMessages() {
    Api.getMessages([self.botName!, self.botName!, self.botName!],
                    onSuccess: { (chat) in
                      if let messages = chat[self.botName!] {
                        for var message in messages {
                          let sender = (Int(message["me"]!)! == 1) ? self.senderId : self.botName!
                          let date = dateFor(message["date"]!)
                          let text = message["text"]
                          let jsqMessage = JSQMessage(senderId: sender, senderDisplayName: sender, date: date, text: text)
                          self.messages += [jsqMessage]
                        }
                      }
                      self.reloadMessagesView()
                    },
                    onForbidden: {
                      print("onSuccess")
                    }, onError: {
                      print("onError")
                    }
    )
  }
  
  func setup() {
    self.senderId = UIDevice.currentDevice().identifierForVendor?.UUIDString
    self.senderDisplayName = UIDevice.currentDevice().identifierForVendor?.UUIDString
  }
}

//MARK - Data Source
extension ChatViewController {
  
  override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.messages.count
  }
  
  override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
    let data = self.messages[indexPath.row]
    return data
  }
  
  override func collectionView(collectionView: JSQMessagesCollectionView!, didDeleteMessageAtIndexPath indexPath: NSIndexPath!) {
    self.messages.removeAtIndex(indexPath.row)
  }
  
  override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
    
    let data = messages[indexPath.row]
    switch(data.senderId) {
    case self.senderId:
      return self.outgoingBubble
    default:
      return self.incomingBubble
    }
  }
  
  override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
    let data = messages[indexPath.row]
    switch(data.senderId) {
    case self.senderId:
      return nil
    default:
      return JSQMessagesAvatarImage.avatarWithImage(self.botAvatar)
    }
  }
}

//MARK - Toolbar
extension ChatViewController {
  override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
    let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
    self.messages += [message]
    self.finishSendingMessage()
  }
  
  override func didPressAccessoryButton(sender: UIButton!) {
    
  }
}