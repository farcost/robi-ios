//
//  Api.swift
//  robi
//
//  Created by a.skogorev on 07/06/16.
//  Copyright © 2016 givemeapp.co. All rights reserved.
//

import Alamofire
import Foundation

// --------------------------------------------------------

class Api {
  private static let authApiUrl = "http://localhost:8080/v1/"
  private static let apiUrl = "http://localhost:8081/v1/"
  
  private static var email : String?
  private static var tokenRefresh : String?
  private static var tokenAccess : String?
  
  private static let KEY_EMAIL = "userEmail"
  private static let KEY_REFRESH_TOKEN = "userRefreshToken"
  private static let KEY_ACCESS_TOKEN = "userAccessToken"
  
  // --------------------------------------------------------
  // Public member access
  
  static func lastEmail() -> String {
    if let e = self.email {
      return e
    }
    
    let conf = NSUserDefaults.standardUserDefaults()
    if let e = conf.stringForKey(KEY_EMAIL) {
      return e
    }
    
    return ""
  }
  
  // --------------------------------------------------------
  // Auth info auxil
  
  private static func accessHeader() -> [String: String] {
    if (email == nil || tokenAccess == nil) {
      return [:]
    }
    
    return [
      "X-Email": email!,
      "X-Access-Token": tokenAccess!
    ]
  }
  
  private static func saveAuthInfo() {
    let conf = NSUserDefaults.standardUserDefaults()
    
    conf.setValue(email, forKey: KEY_EMAIL)
    conf.setValue(tokenRefresh, forKey: KEY_REFRESH_TOKEN)
    conf.setValue(tokenAccess, forKey: KEY_ACCESS_TOKEN)
    conf.synchronize()
  }
  
  // --------------------------------------------------------
  // Global init
  
  static func globalInit() {
    let conf = NSUserDefaults.standardUserDefaults()
    
    if let e = conf.stringForKey(KEY_EMAIL),
           t = conf.stringForKey(KEY_REFRESH_TOKEN),
           a = conf.stringForKey(KEY_ACCESS_TOKEN) {
      
      email = e
      tokenRefresh = t
      tokenAccess = a
      print("Loaded access info for", e)
    } else {
      print("No access info")
    }
  }
  
  // --------------------------------------------------------
  // Control user
  
  static func loginUser(email: String,
                        password: String,
                        onSuccess: () -> Void,
                        onFree: () -> Void,
                        onForbidden: () -> Void,
                        onError: () -> Void) {
    
    Alamofire.request(.POST, authApiUrl + "login",
      headers: ["X-Email": email, "X-Password": password])
      .validate()
      .responseData { response in
        switch response.result {
        case .Success:
          if (response.response?.statusCode == 204) {
            onFree()
            return
          }
          
          if (response.response == nil) {
            print("Response is nil")
            onError()
            return
          }
          
          let headers = response.response!.allHeaderFields
          let refresh = headers["X-Refresh-Token"] as? String
          let access = headers["X-Access-Token"] as? String
          
          if (refresh == nil || access == nil) {
            print("Refresh or/and access tokens are nil")
            onError()
            return
          }
          
          self.email = email
          self.tokenAccess = access
          self.tokenRefresh = refresh
          saveAuthInfo()
          onSuccess()
          
        case .Failure(let error):
          if (response.response?.statusCode == 403) {
            onForbidden()
            return
          }
          
          print("Failed siging in. Error:", error)
          onError()
        }
    }
  }
  
  private static func refreshAccess(onSuccess: () -> Void,
                                    onForbidden: () -> Void,
                                    onError: () -> Void) {
    
    Alamofire.request(.POST, authApiUrl + "access",
      headers: ["X-Email": email!, "X-Refresh-Token": tokenRefresh!])
      .validate()
      .responseData { response in
        switch response.result {
        case .Success:
          if (response.response == nil) {
            print("Response is nil")
            onError()
            return
          }
          
          let headers = response.response!.allHeaderFields
          let access = headers["X-Access-Token"] as? String
          
          if (access == nil) {
            print("Access tokens is nil")
            onError()
            return
          }
          
          self.tokenAccess = access
          saveAuthInfo()
          onSuccess()
          
        case .Failure(let error):
          if (response.response?.statusCode == 403) {
            onForbidden()
            return
          }
          
          print("Failed refreshing access token. Error:", error)
          onError()
        }
    }
  }
  
  static func registerUser(email: String,
                           password: String,
                           onSuccess: () -> Void,
                           onInUse: () -> Void,
                           onError: () -> Void) {
    
    Alamofire.request(.POST, authApiUrl + "register",
      headers: ["X-Email": email, "X-Password": password])
      .validate()
      .responseData { response in
        switch response.result {
        case .Success:
          if (response.response == nil) {
            print("Response is nil")
            onError()
            return
          }
          
          let headers = response.response!.allHeaderFields
          let refresh = headers["X-Refresh-Token"] as? String
          let access = headers["X-Access-Token"] as? String
          
          if (refresh == nil || access == nil) {
            print("Refresh or/and access tokens are nil")
            onError()
            return
          }
          
          self.email = email
          self.tokenRefresh = refresh
          self.tokenAccess = access
          saveAuthInfo()
          onSuccess()
          
        case .Failure(let error):
          if (response.response?.statusCode == 403) {
            onInUse()
            return
          }
          
          print("Failed siging up. Error:", error)
          onError()
        }
    }
  }
  
  static func isAccessAvailable(onSuccess: () -> Void,
                                onForbidden: () -> Void,
                                onError: () -> Void) {
    
    if (email == nil || tokenRefresh == nil) {
      onForbidden()
      return
    }
    
    if (tokenAccess == nil) {
      refreshAccess(onSuccess,
                    onForbidden: onForbidden,
                    onError: onError)
      
    } else {
      // touch
      Alamofire.request(.POST, authApiUrl + "touch",
        headers: accessHeader())
        .validate()
        .responseData { response in
          switch response.result {
          case .Success:
            onSuccess()
            
          case .Failure(let error):
            if (response.response?.statusCode == 403) {
              refreshAccess(onSuccess,
                onForbidden: onForbidden,
                onError: onError)
              return
            }
            
            print("Failed checking access. Error:", error)
            onError()
          }
      } // touch
    }
    
  }
  
  // --------------------------------------------------------
  // Get bots
  
  static func getContacts(onSuccess: (contacts : [String: [String: String]]) -> Void,
                          onForbidden: () -> Void,
                          onError: () -> Void) {
    
    Alamofire.request(.GET, apiUrl + "getContacts",
      headers: accessHeader())
      .validate()
      .responseJSON { response in
        switch response.result {
        case .Success(let js):
          let response = js as? [String: [String : String]]
          if (response == nil) {
            print("Invalid json format:", js)
            onError()
            return
          }
          
          onSuccess(contacts: response!)
        case .Failure(let error):
          if (response.response?.statusCode == 403) {
            onForbidden()
            return
          }
          
          print("Failed getting contact list:", error)
          onError()
        }
    }
  }
  
  static func getMyBots() {
  }
  
  static func searchBot() {
  }
  
  // --------------------------------------------------------
  // Communication bots
  
  static func getMessages(contacts: [String],
                          onSuccess: (chat : [String: [[String: String]]]) -> Void,
                          onForbidden: () -> Void,
                          onError: () -> Void) {
    
    Alamofire.request(.GET, apiUrl + "getMessages",
      headers: accessHeader(),
      parameters: ["contacts" : contacts])
      .validate()
      .responseJSON { response in
        switch response.result {
        case .Success(let js):
          let response = js as? [String: [[String: String]]]
          if (response == nil) {
            print("Invalid json format:", js)
            onError()
            return
          }
          
          onSuccess(chat: response!)
        case .Failure(let error):
          if (response.response?.statusCode == 403) {
            onForbidden()
            return
          }
          
          print("Failed getting messages:", error)
          onError()
        }
    }
  }
  
  static func sendMessage() {
  }
  
  // --------------------------------------------------------
  // Control bots
  
  static func createBot() {
  }
  
  static func deleteBot() {
  }
  
  // --------------------------------------------------------
  // Control resources
  
  static func getImage(url: String,
                       onSuccess: (image: UIImage) -> Void,
                       onError: () -> Void) -> Request {
    
    return Alamofire.request(.GET, url)
      .validate()
      .responseData { response in
        switch response.result {
        case .Success:
          if let d = response.data {
            if let i = UIImage(data: d) {
              onSuccess(image: i)
              return
            } else {
              print("Invalid image body.", response.request)
            }
          } else {
            print("Response data is nil.", response.request)
          }
          
        case .Failure(let error):
          print("Failed getting image:", error)
        }
        
        onError()
    }
  }
}
