//
//  LoginViewController.swift
//  robi
//
//  Created by a.skogorev on 07/06/16.
//  Copyright © 2016 givemeapp.co. All rights reserved.
//

import UIKit

// --------------------------------------------------------

class LoginViewController : UIViewController {
  @IBOutlet weak var btnSignInUp: UIButton!
  
  @IBOutlet weak var txtFieldEmail: UITextField!
  @IBOutlet weak var txtFieldPassword: UITextField!
  
  var loadingIndicator : UIActivityIndicatorView?
  
  // --------------------------------------------------------
  // Initialization
  
  override func viewDidLoad() {
    print("Loading login page")
    super.viewDidLoad()
    btnSignInUp.addTarget(self, action:#selector(onButtonSignInUpAction), forControlEvents:.TouchUpInside)
  }
  
  func back() {
    print("Dismissing login page")
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  // --------------------------------------------------------
  // Validation
  
  func isValidEmail(testStr: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(testStr)
  }
  
  func isValidPassword(testStr: String) -> Bool {
    return testStr.characters.count >= 6
  }
  
  // --------------------------------------------------------
  // Alerting
  
  func errorMessage(msg: String) {
    let ac: UIAlertController = UIAlertController(title: nil,
                                                  message: msg,
                                                  preferredStyle: UIAlertControllerStyle.Alert)
    
    ac.addAction(UIAlertAction(title: "Done", style:UIAlertActionStyle.Default, handler:nil))
    self.presentViewController(ac, animated: true, completion: nil)
    print("Alert shown:", msg)
  }
  
  func loadingIndicatorStart() {
    if (loadingIndicator == nil) {
      loadingIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 40, 40))
      loadingIndicator!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
      loadingIndicator!.center = self.view.center
      loadingIndicator!.startAnimating()
      loadingIndicator!.hidesWhenStopped = true
      self.view.addSubview(loadingIndicator!)
    } else {
      loadingIndicator!.startAnimating()
    }
  }
  
  func loadingIndicatorStop() {
    loadingIndicator?.stopAnimating()
  }
  
  // --------------------------------------------------------
  // Actions
  
  func onButtonSignInUpAction(sender: UIButton!) {
    let email : String? = txtFieldEmail.text
    let pass : String? = txtFieldPassword.text
    
    if (email == nil || pass == nil) {
      return
    }
    
    if (email == nil || !isValidEmail(email!)) {
      print("Invalid email:", email)
      self.errorMessage("Invalid email format")
      return
    }
    
    if (pass == nil || !isValidPassword(pass!)) {
      print("Invalid password")
      self.errorMessage("The password must be longer than 6 characters")
      return
    }
    
    loadingIndicatorStart()
    
    Api.loginUser(email!,
                  password: pass!,
                  
                  onSuccess: { () -> Void in
                    print("Signed in:", email)
                    self.back()
                  },
                  
                  onFree: { () -> Void in
                    print("Free email. Signing up...", email)
                    
                    Api.registerUser(email!,
                                     password: pass!,
                      
                                     onSuccess: { () -> Void in
                                       print("Signed up")
                                       self.back()
                                     },
                                      
                                     onInUse: { () -> Void in
                                       print("Email already in use. Rare case because we try to register free email", email)
                                       self.loadingIndicatorStop()
                                       self.errorMessage("Email already in use")
                                     },
                                      
                                     onError: { () -> Void in
                                       print("Failed registering free email", email)
                                       self.loadingIndicatorStop()
                                       self.errorMessage("Failed signing up to the server")
                                     }
                    ) // registerUser
                  },
                  
                  onForbidden: { () -> Void in
                    print("Invalid login info", email)
                    self.loadingIndicatorStop()
                    self.errorMessage("Invalid email or password")
                  },
                  
                  onError: { () -> Void in
                    print("Failed singing in", email)
                    self.loadingIndicatorStop()
                    self.errorMessage("Failed signing in to the server")
                  }
    ) // loginUser
  }
}
